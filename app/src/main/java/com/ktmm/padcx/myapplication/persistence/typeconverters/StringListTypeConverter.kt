package com.ktmm.padcx.myapplication.persistence.typeconverters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ktmm.padcx.myapplication.data.vos.ScoresAndReviewsVO

class StringListTypeConverter {
    @TypeConverter
    fun toString(scoresAndReviewsVO: ArrayList<String>): String {
        return Gson().toJson(scoresAndReviewsVO)
    }

    @TypeConverter
    fun toStringList(stringListJson: String): ArrayList<String> {
        val stringListType = object : TypeToken<ArrayList<String>>() {}.type
        return Gson().fromJson(stringListJson, stringListType)
    }
}