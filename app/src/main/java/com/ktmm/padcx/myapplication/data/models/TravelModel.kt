package com.ktmm.padcx.myapplication.data.models

import com.ktmm.padcx.myapplication.data.vos.CountryVO
import com.ktmm.padcx.myapplication.data.vos.TourVO
import com.ktmm.padcx.myapplication.data.vos.TravelListVO
import io.reactivex.Observable

interface TravelModel {

    fun getAllCountriesAndTours(): Observable<TravelListVO>

    fun getCountryById(id: Int) : CountryVO
    fun getTourById(id: Int) : TourVO
}