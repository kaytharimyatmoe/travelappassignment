package com.ktmm.padcx.myapplication.delegates

interface CountryDelegate {
    fun onTapCountry(id: Int)
}