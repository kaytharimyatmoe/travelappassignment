package com.ktmm.padcx.myapplication.data.vos

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.ktmm.padcx.myapplication.persistence.typeconverters.ScoresAndReviewsTypeConverter
import com.ktmm.padcx.myapplication.persistence.typeconverters.StringListTypeConverter

@Entity(tableName = "country")
@TypeConverters(ScoresAndReviewsTypeConverter::class, StringListTypeConverter::class)
data class CountryVO (
    @PrimaryKey (autoGenerate = true)
    val id: Int = 0,
    @SerializedName("name") val name: String = "",
    @SerializedName("description") val description: String = "",
    @SerializedName("location") val location: String = "",
    @SerializedName("average_rating") val averageRating: Double = 0.0,
    @SerializedName("scores_and_reviews") val scoresAndReviewVOS: ArrayList<ScoresAndReviewsVO>? = null,
    @SerializedName("photos") val photos: ArrayList<String>? = null
): TravelVO()