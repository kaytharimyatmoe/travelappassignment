package com.ktmm.padcx.myapplication.persistence.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ktmm.padcx.myapplication.data.vos.CountryVO
import com.ktmm.padcx.myapplication.data.vos.TourVO
import com.ktmm.padcx.myapplication.persistence.daos.TravelDao

@Database(entities = [CountryVO::class, TourVO::class], version = 1, exportSchema = false)
abstract class TravelDB: RoomDatabase() {
    companion object {
        const val DBNAME = "PADC_X_TRAVEL.DB"
        var dbInstance: TravelDB? = null

        fun getDBInstance(context: Context): TravelDB {
            when (dbInstance) {
                null -> {
                    dbInstance = Room.databaseBuilder(context, TravelDB::class.java, DBNAME)
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return dbInstance!!
        }
    }

    abstract fun travelDao() : TravelDao
}