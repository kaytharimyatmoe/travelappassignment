package com.ktmm.padcx.myapplication.delegates

interface TourDelegate {
    fun onTapTour(id: Int)
}