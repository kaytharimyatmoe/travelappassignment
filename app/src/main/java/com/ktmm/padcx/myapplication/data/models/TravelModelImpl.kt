package com.ktmm.padcx.myapplication.data.models

import com.ktmm.padcx.myapplication.data.vos.CountryVO
import com.ktmm.padcx.myapplication.data.vos.TourVO
import com.ktmm.padcx.myapplication.data.vos.TravelListVO
import com.ktmm.padcx.myapplication.network.responses.GetAllCountriesResponse
import com.ktmm.padcx.myapplication.network.responses.GetAllToursResponse
import io.reactivex.Observable
import io.reactivex.functions.BiFunction

object TravelModelImpl : TravelModel, BaseModel(){

    override fun getAllCountriesAndTours(): Observable<TravelListVO> =
            Observable.zip(
                mTravelApi.getAllCountries(),
                mTravelApi.getAllTours(),
                BiFunction<GetAllCountriesResponse, GetAllToursResponse, TravelListVO> { countries, tours ->
                    mTravelDB.travelDao().deleteAllCountries()
                    mTravelDB.travelDao().deleteAllTours()
                    mTravelDB.travelDao().insertAllCountries(countries.data ?: listOf())
                    mTravelDB.travelDao().insertAllTours(tours.data ?: listOf())
                    return@BiFunction TravelListVO(
                        mTravelDB.travelDao().getAllCountries(),
                        mTravelDB.travelDao().getAllTours())
                }
            )

    override fun getCountryById(id: Int): CountryVO {
        val a = mTravelDB.travelDao().getAllCountries()
        val vo = mTravelDB.travelDao().getCountryById(id)
        return vo
    }

    override fun getTourById(id: Int): TourVO =
        mTravelDB.travelDao().getTourById(id)

}