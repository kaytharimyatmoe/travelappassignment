package com.ktmm.padcx.myapplication.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.ktmm.padcx.myapplication.R
import com.ktmm.padcx.myapplication.data.vos.CountryVO
import com.ktmm.padcx.myapplication.delegates.CountryDelegate
import com.ktmm.padcx.myapplication.views.viewholders.CountryViewHolder

class CountriesListAdapter(private val delegate: CountryDelegate) : BaseRecyclerAdapter<CountryViewHolder,CountryVO>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.rv_horizontal_item, parent, false)
        return CountryViewHolder(view, delegate)
    }
}