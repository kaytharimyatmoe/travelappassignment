package com.ktmm.padcx.myapplication.views.viewpods

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_pod_service.view.*

class ServiceViewPod @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    override fun onFinishInflate() {
        super.onFinishInflate()
    }
    fun setServiceData(serviceTitle: String, @DrawableRes serviceImage: Int){
        ivServiceViewPod.setImageResource(serviceImage)
//        Glide.with(context)
//            .load(serviceImage)
//            .into(ivServiceViewPod)
        tvServiceViewPod.text = serviceTitle
    }
}