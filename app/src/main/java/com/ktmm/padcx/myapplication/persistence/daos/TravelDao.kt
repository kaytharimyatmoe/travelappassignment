package com.ktmm.padcx.myapplication.persistence.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ktmm.padcx.myapplication.data.vos.CountryVO
import com.ktmm.padcx.myapplication.data.vos.TourVO
import io.reactivex.Observable

@Dao
interface TravelDao {

    @Query("select * from country")
    fun getAllCountries(): List<CountryVO>

    @Query("select * from tour")
    fun getAllTours(): List<TourVO>

    @Query("select * from country where id = :id")
    fun getCountryById(id: Int) : CountryVO

    @Query("select * from tour where id = :id")
    fun getTourById(id: Int) : TourVO

    @Query("delete from country")
    fun deleteAllCountries()

    @Query("delete from tour")
    fun deleteAllTours()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllCountries (countries: List<CountryVO>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllTours (tours: List<TourVO>)
}