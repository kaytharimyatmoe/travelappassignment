package com.ktmm.padcx.myapplication.utils

const val CODE_RESPONSE_OK = 200

const val BASE_URL = "https://e1314c36-5305-4773-90b6-d3032291d1bf.mock.pstmn.io"

const val GET_TOURS = "/getAllTours"
const val GET_COUNTRIES = "/getAllCountries"

const val PARAM_ACCESS_TOKEN = "access_token"

const val DUMMY_ACCESS_TOKEN = "12345dfsgfgs"

//Error Message
const val EM_NO_INTERNET_CONNECTION = "No Internet Connection.."