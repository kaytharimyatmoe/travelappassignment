package com.ktmm.padcx.myapplication.network.responses

import com.google.gson.annotations.SerializedName
import com.ktmm.padcx.myapplication.data.vos.CountryVO

data class GetAllCountriesResponse (
    @SerializedName("code") val code: Int = 0,
    @SerializedName("message") val message: String = "",
    @SerializedName("data") val data: ArrayList<CountryVO>? = null
) {
    fun isResponseOk() = (code == 200) && (data != null)
}