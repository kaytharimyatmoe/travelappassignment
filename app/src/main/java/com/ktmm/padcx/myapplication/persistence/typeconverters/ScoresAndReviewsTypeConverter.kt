package com.ktmm.padcx.myapplication.persistence.typeconverters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ktmm.padcx.myapplication.data.vos.ScoresAndReviewsVO

class ScoresAndReviewsTypeConverter {
    @TypeConverter
    fun toString(scoresAndReviewsVO: ArrayList<ScoresAndReviewsVO>): String {
        return Gson().toJson(scoresAndReviewsVO)
    }

    @TypeConverter
    fun toList(scoresAndReviewsJson: String): ArrayList<ScoresAndReviewsVO> {
        val publicationType = object : TypeToken<ArrayList<ScoresAndReviewsVO>>() {}.type
        return Gson().fromJson(scoresAndReviewsJson, publicationType)
    }
}