package com.ktmm.padcx.myapplication.views.viewholders

import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ktmm.padcx.myapplication.data.vos.TourVO
import com.ktmm.padcx.myapplication.delegates.TourDelegate
import kotlinx.android.synthetic.main.rv_vertical_item.view.*

class TourViewHolder(itemView: View,
                     delegate: TourDelegate)
    : BaseViewHolder<TourVO>(itemView){
    override fun bindData(data: TourVO) {
        mData = data
        Glide.with(itemView.context)
            .load(data.photos?.get(0))
            .apply(RequestOptions().centerCrop())
            .into(itemView.ivTour)
        itemView.tvTourName.text = data.name
        itemView.tvTourRating.text = data.averageRating.toString()
    }

    init {
        itemView.setOnClickListener { delegate.onTapTour(adapterPosition + 1) }
    }
}