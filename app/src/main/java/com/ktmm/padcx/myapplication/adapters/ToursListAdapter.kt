package com.ktmm.padcx.myapplication.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.ktmm.padcx.myapplication.R
import com.ktmm.padcx.myapplication.data.vos.TourVO
import com.ktmm.padcx.myapplication.delegates.TourDelegate
import com.ktmm.padcx.myapplication.views.viewholders.TourViewHolder

class ToursListAdapter(private val delegate: TourDelegate) : BaseRecyclerAdapter<TourViewHolder,TourVO>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TourViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.rv_vertical_item, parent, false)
        return TourViewHolder(view, delegate)
    }
}