package com.ktmm.padcx.myapplication.views.viewholders

import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ktmm.padcx.myapplication.data.vos.CountryVO
import com.ktmm.padcx.myapplication.delegates.CountryDelegate
import kotlinx.android.synthetic.main.rv_horizontal_item.view.*

class CountryViewHolder(itemView: View,
                        delegate: CountryDelegate)
    : BaseViewHolder<CountryVO>(itemView){
    override fun bindData(data: CountryVO) {
        mData = data
        Glide.with(itemView.context)
            .load(data.photos?.get(0))
            .apply(RequestOptions().centerCrop())
            .into(itemView.ivCountry)
        itemView.tvCountryName.text = data.name
        itemView.tvCountryRating.text = data.averageRating.toString()
    }

    init {
        itemView.setOnClickListener { delegate.onTapCountry(adapterPosition + 1) }
    }
}