package com.ktmm.padcx.myapplication.data.vos

data class TravelListVO (
    val countryList: List<CountryVO>,
    val tourList: List<TourVO>
)