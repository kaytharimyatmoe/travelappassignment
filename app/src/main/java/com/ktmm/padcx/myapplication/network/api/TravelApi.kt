package com.ktmm.padcx.myapplication.network.api

import com.ktmm.padcx.myapplication.network.responses.GetAllCountriesResponse
import com.ktmm.padcx.myapplication.network.responses.GetAllToursResponse
import com.ktmm.padcx.myapplication.utils.GET_COUNTRIES
import com.ktmm.padcx.myapplication.utils.GET_TOURS
import io.reactivex.Observable
import retrofit2.http.GET

interface TravelApi {

    @GET(GET_COUNTRIES)
    fun getAllCountries(): Observable<GetAllCountriesResponse>

    @GET(GET_TOURS)
    fun getAllTours(): Observable<GetAllToursResponse>
}