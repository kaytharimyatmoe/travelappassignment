package com.ktmm.padcx.myapplication.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ktmm.padcx.myapplication.R
import com.ktmm.padcx.myapplication.data.models.TravelModelImpl
import com.ktmm.padcx.myapplication.data.vos.CountryVO
import com.ktmm.padcx.myapplication.data.vos.TourVO
import com.ktmm.padcx.myapplication.data.vos.TravelVO
import com.ktmm.padcx.myapplication.views.viewpods.RatingViewPod
import com.ktmm.padcx.myapplication.views.viewpods.ServiceViewPod
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : BaseActivity() {
    private var travelId: Int = 0
    private var travelTableName: String = ""
    var travelVO: TravelVO? = null

    private val mTravelModel = TravelModelImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        travelId = intent.getIntExtra(exId, 0)
        travelTableName = intent.getStringExtra(exDatabaseTableName) ?: ""

        getTravelVO()
        bindData()
    }

    private fun bindData(){
        when (travelTableName){
            countryTable -> bindCountry(travelVO as CountryVO)
            tourTable -> bindTour(travelVO as TourVO)
        }
    }

    private fun bindCountry(countryVO: CountryVO){
        Glide.with(this)
            .load(countryVO.photos!![1])
            .apply(RequestOptions().fitCenter())
            .into(ivDetailImage)

        tvDetailName.text = countryVO.name
        tvDetailLocation.text = countryVO.location
        ratingBarDetail.rating = countryVO.averageRating.toFloat()
        setUpViewPods()

        tvTourDetailDescription.text = countryVO.description
    }

    private fun bindTour(tourVO: TourVO){
        Glide.with(this)
            .load(tourVO.photos!![1])
            .apply(RequestOptions().fitCenter())
            .into(ivDetailImage)

        tvDetailName.text = tourVO.name
        tvDetailLocation.text = tourVO.location
        ratingBarDetail.rating = tourVO.averageRating.toFloat()
        setUpViewPods()

        tvTourDetailDescription.text = tourVO.description
    }

    private fun setUpViewPods(){
        initializeViewPods()
        setServiceViewPodData()
        setRatingViewPodData()
    }

    private fun setRatingViewPodCountry(countryVO: CountryVO){
        countryVO.scoresAndReviewVOS?.let {it->
            ratingViewPodBooking.setRatingData(
                R.drawable.ic_book,
                it[0].name,
                it[0].score, it[0].maxScore, it[0].totalReviews
            )

            ratingViewPodBooking.setRatingData(
                R.drawable.ic_rate,
                it[1].name,
                it[1].score, it[1].maxScore, it[1].totalReviews
            )
        }
    }

    private fun setRatingViewPodTour(tourVO: TourVO){
        tourVO.scoresAndReviewVOS?.let {it->
            ratingViewPodBooking.setRatingData(
                R.drawable.ic_book,
                it[0].name,
                it[0].score, it[0].maxScore, it[0].totalReviews
            )

            ratingViewPodBooking.setRatingData(
                R.drawable.ic_rate,
                it[1].name,
                it[1].score, it[1].maxScore, it[1].totalReviews
            )
        }
    }

    private fun setRatingViewPodData(){
        when(travelTableName) {
            countryTable -> setRatingViewPodCountry(travelVO as CountryVO)
            else -> setRatingViewPodTour(travelVO as TourVO)
        }
    }

    private fun setServiceViewPodData(){
        serviceViewPodWifi.setServiceData(getString(R.string.text_wifi), R.drawable.ic_wifi)
        serviceViewPodBeach.setServiceData(getString(R.string.text_beach), R.drawable.ic_sand_beach)
        serviceViewPodCoastline.setServiceData(getString(R.string.text_coastline), R.drawable.ic_first_coastline)
        serviceViewPodBar.setServiceData(getString(R.string.text_restaurant), R.drawable.ic_bar_restaurant)
    }

    private fun initializeViewPods() {
        serviceViewPodWifi = svpWifi as ServiceViewPod
        serviceViewPodBeach = svpBeach as ServiceViewPod
        serviceViewPodCoastline = svpCoastline as ServiceViewPod
        serviceViewPodBar = svpBar as ServiceViewPod

        ratingViewPodBooking = rvpBooking as RatingViewPod
        ratingViewPodHotelOut = rvpHotelOut as RatingViewPod
    }

    private fun getTravelVO(){
        when (travelTableName){
            countryTable ->{
                travelVO = mTravelModel.getCountryById(travelId)
            }
            tourTable ->
                travelVO = mTravelModel.getTourById(travelId)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        MenuInflater(this).inflate(R.menu.detail_activity_menu, menu)
        return true
    }

    companion object {
        const val exId = "extra-Id"
        const val exDatabaseTableName = "extra-database-table-name"

        const val countryTable = "country"
        const val tourTable = "tour"

        fun newIntent(context: Context, id: Int, tableName: String): Intent{
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(exId, id)
            intent.putExtra(exDatabaseTableName, tableName)
            return intent
        }
    }

    private lateinit var serviceViewPodWifi: ServiceViewPod
    private lateinit var serviceViewPodBeach: ServiceViewPod
    private lateinit var serviceViewPodCoastline: ServiceViewPod
    private lateinit var serviceViewPodBar: ServiceViewPod

    private lateinit var ratingViewPodBooking: RatingViewPod
    private lateinit var ratingViewPodHotelOut: RatingViewPod
}
