package com.ktmm.padcx.myapplication.views.viewpods

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.DrawableRes
import androidx.cardview.widget.CardView
import kotlinx.android.synthetic.main.view_pod_rating.view.*

class RatingViewPod @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : CardView(context, attrs, defStyleAttr) {
    override fun onFinishInflate() {
        super.onFinishInflate()
    }

    fun setRatingData(@DrawableRes ratingIcon: Int,
                      ratingName: String,
                      ratingScore: Double,
                      ratingMaxScore: Double,
                      ratingTotalReview: Int){
        ivRatingViewPod.setImageResource(ratingIcon)
        tvRatingTypeRatingViewPod.text = ratingName
        tvRatingRatingViewPod.text = String.format("%f/%f", ratingScore, ratingMaxScore)
        tvRatingBasedOnRatingViewPod.text = String.format("Based on %d reviews", ratingTotalReview)
    }
}